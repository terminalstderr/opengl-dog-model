/*=========================================================================

Program:   Visualization Toolkit
Module:    SpecularSpheres.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
//
// This examples demonstrates the effect of specular lighting.
//
#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkInteractorStyle.h"
#include "vtkObjectFactory.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkLight.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkJPEGReader.h"
#include "vtkImageData.h"

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>

#include <vector>

class Triangle
{
  public:
    double         X[3];
    double         Y[3];
    double         Z[3];
};

class vtk441Mapper : public vtkOpenGLPolyDataMapper
{
  protected:
    GLuint displayList;
    bool   initialized;

  public:
    vtk441Mapper()
    {
      initialized = false;
    }

    void
      RemoveVTKOpenGLStateSideEffects()
      {
	float Info[4] = { 0, 0, 0, 1 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, Info);
	float ambient[4] = { 1,1, 1, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	float diffuse[4] = { 1, 1, 1, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	float specular[4] = { 1, 1, 1, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
      }


    void SetupLight(void)
    {
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      GLfloat diffuse0[4] = { 0.6, 0.6, 0.6, 1 };
      GLfloat ambient0[4] = { 0.2, 0.2, 0.2, 1 };
      GLfloat specular0[4] = { 0.0, 0.0, 0.0, 1 };
      GLfloat pos0[4] = { 0, -.707, -0.707, 0 };
      glLightfv(GL_LIGHT0, GL_POSITION, pos0);
      glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0);
      glLightfv(GL_LIGHT0, GL_AMBIENT, ambient0);
      glLightfv(GL_LIGHT0, GL_SPECULAR, specular0);
      glEnable(GL_LIGHT1);
      GLfloat pos1[4] = { .707, -.707, 0 };
      glLightfv(GL_LIGHT1, GL_POSITION, pos1);
      glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0);
      glLightfv(GL_LIGHT1, GL_AMBIENT, ambient0);
      glLightfv(GL_LIGHT1, GL_SPECULAR, specular0);
      glDisable(GL_LIGHT2);
      glDisable(GL_LIGHT3);
      glDisable(GL_LIGHT5);
      glDisable(GL_LIGHT6);
      glDisable(GL_LIGHT7);
    }
};

class vtk441MapperPart3 : public vtk441Mapper
{
  public:
    static vtk441MapperPart3 *New();

    GLuint displayList;
    bool   initialized;
    double headSize; // absolute terms
    double eyeballSize; // as proportion of headSize
    double eyeHeight; // as proportion of radius above center of sphere
    double pupilSize; // as proportion of eyeballSize
    double eyeAngle; // from center-line, in degrees.  

    vtk441MapperPart3()
    {
      initialized = false;
      headSize = 4;
      eyeballSize = 0.2;
      eyeHeight = 0.25;
      pupilSize = 0.25;
      eyeAngle = 22;
    }

    void DrawCylinder()
    {
      glPushMatrix();
      glRotatef(90, 1,0,0);
      int nfacets = 30;
      glBegin(GL_TRIANGLES);
      for (int i = 0 ; i < nfacets ; i++)
      {
	double angle = 3.14159*2.0*i/nfacets;
	double nextAngle = (i == nfacets-1 ? 0 : 3.14159*2.0*(i+1)/nfacets);
	glNormal3f(0,0,1);
	glVertex3f(0, 0, 1);
	glVertex3f(cos(angle), sin(angle), 1);
	glVertex3f(cos(nextAngle), sin(nextAngle), 1);
	glNormal3f(0,0,-1);
	glVertex3f(0, 0, 0);
	glVertex3f(cos(angle), sin(angle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 0);
      }
      glEnd();
      glBegin(GL_QUADS);
      for (int i = 0 ; i < nfacets ; i++)
      {
	double angle = 3.14159*2.0*i/nfacets;
	double nextAngle = (i == nfacets-1 ? 0 : 3.14159*2.0*(i+1)/nfacets);
	glNormal3f(cos(angle), sin(angle), 0);
	glVertex3f(cos(angle), sin(angle), 1);
	glVertex3f(cos(angle), sin(angle), 0);
	glNormal3f(cos(nextAngle), sin(nextAngle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 1);
      }
      glEnd();
      glPopMatrix();
    }

    void DrawCylinderShell()
    {
      float rim_thickness = 0.85;
      glPushMatrix();
      glRotatef(90, 1,0,0);
      int nfacets = 30;
      glBegin(GL_QUADS);
      for (int i = 0 ; i < nfacets ; i++)
      {
	double angle = 3.14159*2.0*i/nfacets;
	double nextAngle = (i == nfacets-1 ? 0 : 3.14159*2.0*(i+1)/nfacets);
	glNormal3f(0, 0, 1);
	//glVertex3f(0.8, 0.8, 1);
	glVertex3f(cos(angle)*rim_thickness, sin(angle)*rim_thickness, 1);
	glVertex3f(cos(angle), sin(angle), 1);
	glVertex3f(cos(nextAngle), sin(nextAngle), 1);
	glVertex3f(cos(nextAngle)*rim_thickness, sin(nextAngle)*rim_thickness, 1);

	glNormal3f(0, 0,-1);
	glVertex3f(cos(angle)*rim_thickness, sin(angle)*rim_thickness, 0);
	glVertex3f(cos(angle), sin(angle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 0);
	glVertex3f(cos(nextAngle)*rim_thickness, sin(nextAngle)*rim_thickness, 0);
      }
      glEnd();
      glBegin(GL_QUADS);
      for (int i = 0 ; i < nfacets ; i++)
      {
	double angle = 3.14159*2.0*i/nfacets;
	double nextAngle = (i == nfacets-1 ? 0 : 3.14159*2.0*(i+1)/nfacets);
	glNormal3f(cos(angle), sin(angle), 0);
	glVertex3f(cos(angle), sin(angle), 1);
	glVertex3f(cos(angle), sin(angle), 0);
	glNormal3f(cos(nextAngle), sin(nextAngle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 0);
	glVertex3f(cos(nextAngle), sin(nextAngle), 1);
      }
      glEnd();
      glPopMatrix();
    }

    void SplitTriangle(std::vector<Triangle> &list, std::vector<Triangle> &output)
    {
      output.resize(4*list.size());
      for (unsigned int i = 0 ; i < list.size() ; i++)
      {
	double mid1[3], mid2[3], mid3[3];
	mid1[0] = (list[i].X[0]+list[i].X[1])/2;
	mid1[1] = (list[i].Y[0]+list[i].Y[1])/2;
	mid1[2] = (list[i].Z[0]+list[i].Z[1])/2;
	mid2[0] = (list[i].X[1]+list[i].X[2])/2;
	mid2[1] = (list[i].Y[1]+list[i].Y[2])/2;
	mid2[2] = (list[i].Z[1]+list[i].Z[2])/2;
	mid3[0] = (list[i].X[0]+list[i].X[2])/2;
	mid3[1] = (list[i].Y[0]+list[i].Y[2])/2;
	mid3[2] = (list[i].Z[0]+list[i].Z[2])/2;
	output[4*i+0].X[0] = list[i].X[0];
	output[4*i+0].Y[0] = list[i].Y[0];
	output[4*i+0].Z[0] = list[i].Z[0];
	output[4*i+0].X[1] = mid1[0];
	output[4*i+0].Y[1] = mid1[1];
	output[4*i+0].Z[1] = mid1[2];
	output[4*i+0].X[2] = mid3[0];
	output[4*i+0].Y[2] = mid3[1];
	output[4*i+0].Z[2] = mid3[2];
	output[4*i+1].X[0] = list[i].X[1];
	output[4*i+1].Y[0] = list[i].Y[1];
	output[4*i+1].Z[0] = list[i].Z[1];
	output[4*i+1].X[1] = mid2[0];
	output[4*i+1].Y[1] = mid2[1];
	output[4*i+1].Z[1] = mid2[2];
	output[4*i+1].X[2] = mid1[0];
	output[4*i+1].Y[2] = mid1[1];
	output[4*i+1].Z[2] = mid1[2];
	output[4*i+2].X[0] = list[i].X[2];
	output[4*i+2].Y[0] = list[i].Y[2];
	output[4*i+2].Z[0] = list[i].Z[2];
	output[4*i+2].X[1] = mid3[0];
	output[4*i+2].Y[1] = mid3[1];
	output[4*i+2].Z[1] = mid3[2];
	output[4*i+2].X[2] = mid2[0];
	output[4*i+2].Y[2] = mid2[1];
	output[4*i+2].Z[2] = mid2[2];
	output[4*i+3].X[0] = mid1[0];
	output[4*i+3].Y[0] = mid1[1];
	output[4*i+3].Z[0] = mid1[2];
	output[4*i+3].X[1] = mid2[0];
	output[4*i+3].Y[1] = mid2[1];
	output[4*i+3].Z[1] = mid2[2];
	output[4*i+3].X[2] = mid3[0];
	output[4*i+3].Y[2] = mid3[1];
	output[4*i+3].Z[2] = mid3[2];
      }
    }

    void DrawSphere()
    {
      int recursionLevel = 3;
      Triangle t;
      t.X[0] = 1;
      t.Y[0] = 0;
      t.Z[0] = 0;
      t.X[1] = 0;
      t.Y[1] = 1;
      t.Z[1] = 0;
      t.X[2] = 0;
      t.Y[2] = 0;
      t.Z[2] = 1;
      std::vector<Triangle> list;
      std::vector<Triangle> res;
      list.push_back(t);
      // XXX We are having troubles copying from one vector to another...
      // Our workaround is to call the SplitTriangles function directly many times.
      SplitTriangle(list, res);
      SplitTriangle(res, list);
      SplitTriangle(list, res);
      SplitTriangle(res, list);
      SplitTriangle(list, res);
      SplitTriangle(res, list);

      // really draw
      for (int octent = 0 ; octent < 8 ; octent++)
      {
	glPushMatrix();
	glRotatef(90*(octent%4), 1, 0, 0);
	if (octent >= 4)
	  glRotatef(180, 0, 0, 1);
	glBegin(GL_TRIANGLES);
	for (unsigned int i = 0 ; i < list.size() ; i++)
	{
	  for (int j = 0 ; j < 3 ; j++)
	  {
	    double ptMag = sqrt(list[i].X[j]*list[i].X[j]+
		list[i].Y[j]*list[i].Y[j]+
		list[i].Z[j]*list[i].Z[j]);
	    glNormal3f(list[i].X[j]/ptMag, list[i].Y[j]/ptMag, list[i].Z[j]/ptMag);
	    glVertex3f(list[i].X[j]/ptMag, list[i].Y[j]/ptMag, list[i].Z[j]/ptMag);
	  }
	}
	glEnd();
	glPopMatrix();
      }
    }

    void Brown(void) { glColor3ub(205, 133, 63); };
    void LightBrown(void) { glColor3ub(245, 222, 179); };
    void DarkBrown(void) { glColor3ub(162, 82, 45); };
    void Pink(void) { glColor3ub(250, 128, 114); };
    void White(void) { glColor3ub(255, 255, 255); };
    void Black(void) { glColor3ub(0, 0, 0); };

    void drawWireCube(int s) {
      glBegin(GL_LINES);
      // FRONT
      glColor3f(1.0,1.0,1.0);
      glVertex3f(-s, -s, -s);
      glVertex3f(s, -s, -s);
      glVertex3f(s, -s, -s);
      glVertex3f(s, s, -s);
      glVertex3f(s, s, -s);
      glVertex3f(-s, s, -s);
      glVertex3f(-s, s, -s);
      glVertex3f(-s, -s, -s);

      // BACK
      glVertex3f(s, s, s);
      glVertex3f(-s, s, s);
      glVertex3f(-s, s, s);
      glVertex3f(-s, -s, s);
      glVertex3f(-s, -s, s);
      glVertex3f(s, -s, s);
      glVertex3f(s, -s, s);
      glVertex3f(s, s, s);

      // CONNECTORS
      glVertex3f(-s, -s, s);
      glVertex3f(-s, -s, -s);
      glVertex3f(-s, s, s);
      glVertex3f(-s, s, -s);
      glVertex3f(s, s, s);
      glVertex3f(s, s, -s);
      glVertex3f(s, -s, s);
      glVertex3f(s, -s, -s);

      glEnd();
    }

    void DrawEyeball(void)
    {
      glPushMatrix();
      White();
      glScalef(eyeballSize, eyeballSize, eyeballSize);
      DrawSphere();
      Black();
      glTranslatef(1-pupilSize/2, 0, 0);
      glScalef(pupilSize, pupilSize, pupilSize);
      DrawSphere();
      glPopMatrix();
    }

    void DrawLeftArm() {
      glPushMatrix();
      glScalef(1,4,0.8);
      DrawCylinder();
      glPopMatrix();
      DrawHand();
    }

    void DrawRightArm() {
      glPushMatrix();
      // Doing the flip via rotation is kind of wack, but it maintains the correct normals
      glRotatef(180,0,1,0);
      DrawLeftArm();
      glPopMatrix();
    }

    void DrawHand() {
      double hand_size[3] = {1.5,1.1,0.8};
      double thumb_size[3] = {0.7, 0.5, 0.5};

      glPushMatrix();
      glScalef(hand_size[0], hand_size[1], hand_size[2]);
      DrawSphere();
      glPopMatrix();

      glPushMatrix();
      glTranslatef(hand_size[0],0.2,0);
      glRotatef(-28,0,0,1);
      glScalef(thumb_size[0], thumb_size[1], thumb_size[2]);
      DrawSphere();
      glPopMatrix();
    }

    void DrawLeg() {
      double leg_size[3] = {1.0, 5.0, 1.0};
      glPushMatrix();

      glPushMatrix();
      glScalef(leg_size[0], leg_size[1], leg_size[2]);
      DrawCylinder();
      glPopMatrix();

      glPushMatrix();
      glTranslatef(0,0,0.5);
      DrawFoot();
      glPopMatrix();

      glPopMatrix();
    }

    // DONE
    void DrawFoot() {
      // One big sphere, three smaller for toes
      // The main part of foot
      double foot_size[3] = {1.8,1,1.5};
      double toe_size[3] = {0.4,0.6,0.5};

      glPushMatrix();
      glScalef(foot_size[0], foot_size[1], foot_size[2]); 
      DrawSphere();
      glPopMatrix();

      glPushMatrix();
      {
	glScalef(toe_size[0], toe_size[1], toe_size[2]); // Scale on stack

	color2();
      	glPushMatrix();
      	glTranslatef(-3,0.4,2); // Left toe
      	DrawSphere();
      	glPopMatrix();

      	glPushMatrix();
      	glTranslatef(0,0.4,2.8); // Middle toe
      	DrawSphere();
      	glPopMatrix();

      	glPushMatrix();
      	glTranslatef(3,0.4,2);
      	DrawSphere();
      	glPopMatrix();
      }
      glPopMatrix();
    }

    void DrawGlasses() {
      double angle = 35;
      double length = 3;
      // Draw two wide cylenders that are 'hollow' and black
      // Drow two long cylenders that are full and black
      Black();

      glPushMatrix();
      glTranslatef(-2,0,0);
      glRotatef(angle,0,1,0); // Rotate 20 around y
      glRotatef(90,1,0,0); // Rotate 90 around x
      glScalef(0.06,length,0.06);
      DrawCylinder();
      glPopMatrix();

      glPushMatrix();
      glTranslatef(2,0,0);
      glRotatef(-angle,0,1,0); // Rotate 20 around y
      glRotatef(90,1,0,0); // Rotate 90 around x
      glScalef(0.06,length,0.06);
      DrawCylinder();
      glPopMatrix();


      glPushMatrix();
      glRotatef(90,1,0,0); // Rotate 90 around x
      glScalef(1.0,0.12,1.2);
      glPushMatrix();
      glTranslatef(1,0,0);
      DrawCylinderShell();
      glPopMatrix();
      glPushMatrix();
      glTranslatef(-1,0,0);
      DrawCylinderShell();
      glPopMatrix();
      glPopMatrix();


      White();
    }

    void DrawTorsoAndMuzzle() {
      glPushMatrix();
      // Face
      color2();
      glScalef(0.8,0.8,0.6);
      DrawSphere();
      glRotatef(-15,1,0,0);
      glTranslatef(0,0,0.8);
      glScalef(0.6, 0.4, 0.6);
      // Muzzle
      color1();
      DrawSphere();
      glRotatef(15,1,0,0);
      glTranslatef(0,0,1.0);
      glScalef(0.3,0.3,0.3);
      // Nose
      Black();
      DrawSphere();
      glPopMatrix();

      // Body
      color2();
      glPushMatrix();
      glTranslatef(0,1.0,0);
      glScalef(1.0,1.0,0.7);
      DrawSphere();
      glPopMatrix();
    }

    void DrawTail() {

      glPushMatrix();
      for (int i = 0; i < 6; i++) {
	glTranslatef(0,1,0);
	glRotatef(10,1,0,0);
	glScalef(0.8, 1.2, 0.8);
	DrawCylinder();
      }
      glPopMatrix();
    }

    void DrawLeftEar() {
      glPushMatrix();
      {
	glRotatef(15,1,0,0);
	glRotatef(30,0,0,1);
	glScalef(0.3,2.5,1.5);
	DrawSphere();
      }
      glPopMatrix();
    }

    void DrawRightEar() {
      glPushMatrix();
      {
	glRotatef(-15,1,0,0);
	glRotatef(-30,0,0,1);
	glScalef(0.3,2.5,1.5);
	DrawSphere();
      }
      glPopMatrix();
    }

    void color1() {
      LightBrown();
    }
    void color2() {
      Brown();
    }

    void DrawDog() {
      double body_size = 6;
      double leg_offset = 6;
      double arm_offset = 0;
      color1();
      glPushMatrix();
      glTranslatef(body_size/3,leg_offset,0);
      DrawLeg();
      glPopMatrix();

      color1();
      glPushMatrix();
      glTranslatef(-body_size/3,leg_offset,0);
      DrawLeg();
      glPopMatrix();

      color1();
      glPushMatrix();
      glTranslatef(-body_size,arm_offset,0);
      glRotatef(35,0,0,1);
      DrawLeftArm();
      glPopMatrix();

      color1();
      glPushMatrix();
      glTranslatef(body_size,arm_offset,0);
      glRotatef(35,0,0,-1);
      DrawRightArm();
      glPopMatrix();

      glPushMatrix();
      glTranslatef(0,-8,0);
      glScalef(body_size*0.8, body_size, body_size*0.8);
      DrawTorsoAndMuzzle();
      glPopMatrix();

      // Glasses eyeballs and ears
      glPushMatrix();
      glTranslatef(0,-body_size*1.6,2.9);
      DrawGlasses();
      {
	glPushMatrix();
	glTranslatef(1,0.3,-0.9);
	glRotatef(270,0,1,0);
	glScalef(5.0,5.0,5.0);
	DrawEyeball();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1,0.3,-0.9);
	glRotatef(270,0,1,0);
	glScalef(5.0,5.0,5.0);
	DrawEyeball();
	glPopMatrix();
      }
      glTranslatef(0,0,-2.9);
      {
	color2();
	glPushMatrix();
	glTranslatef(body_size*0.6,0,0);
	DrawRightEar();
	glPopMatrix();
      }
      {
	color1();
	glPushMatrix();
	glTranslatef(-body_size*0.6,0,0);
	DrawLeftEar();
	glPopMatrix();
      }
      glPopMatrix();

      // Tail
      color1();
      glPushMatrix();
      glScalef(0.5,0.5,0.5);
      glTranslatef(0,0,-6);
      glRotatef(180,0,0,1);
      glRotatef(-90,1,0,0);
      DrawTail();
      glPopMatrix();
    }

    virtual void RenderPiece(vtkRenderer *ren, vtkActor *act)
    {
      RemoveVTKOpenGLStateSideEffects();
      SetupLight();
      glEnable(GL_COLOR_MATERIAL);

      glMatrixMode(GL_MODELVIEW);
      glTranslatef(-8.0,0,0);
      glScalef(1.0,-1.0,1.0);
      DrawDog();
    }

    /*
       virtual void RenderPiece(vtkRenderer *ren, vtkActor *act)
       {
       RemoveVTKOpenGLStateSideEffects();
       SetupLight();
       glEnable(GL_COLOR_MATERIAL);

       glMatrixMode(GL_MODELVIEW);

    // draw head
    Brown();
    glPushMatrix();
    glScalef(headSize, headSize, headSize);
    DrawSphere();

    // draw eyeballs
    double angleRadians = eyeAngle/360.0 * 2 * 3.14159;
    double mag = sqrt(1*1-eyeHeight*eyeHeight)*(1-eyeballSize/2);

    glPushMatrix();
    glTranslatef(mag*cos(angleRadians), mag*sin(angleRadians), eyeHeight);
    DrawEyeball();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(mag*cos(-angleRadians), mag*sin(-angleRadians), eyeHeight);
    DrawEyeball();
    glPopMatrix();

    glPopMatrix(); // body
    }
    */
};

vtkStandardNewMacro(vtk441MapperPart3);

int main()
{
  // Dummy input so VTK pipeline mojo is happy.
  //
  vtkSmartPointer<vtkSphereSource> sphere =
    vtkSmartPointer<vtkSphereSource>::New();
  sphere->SetThetaResolution(100);
  sphere->SetPhiResolution(50);

  vtkSmartPointer<vtk441MapperPart3> win3Mapper =
    vtkSmartPointer<vtk441MapperPart3>::New();
  win3Mapper->SetInputConnection(sphere->GetOutputPort());

  vtkSmartPointer<vtkActor> win3Actor =
    vtkSmartPointer<vtkActor>::New();
  win3Actor->SetMapper(win3Mapper);

  vtkSmartPointer<vtkRenderer> ren3 =
    vtkSmartPointer<vtkRenderer>::New();

  vtkSmartPointer<vtkRenderWindow> renWin =
    vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(ren3);
  ren3->SetViewport(0, 0, 1, 1);

  vtkSmartPointer<vtkRenderWindowInteractor> iren =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);

  // Add the actors to the renderer, set the background and size.
  //
  bool doWindow3 = true;
  if (doWindow3)
    ren3->AddActor(win3Actor);
  ren3->SetBackground(0.3, 0.3, 0.3);
  renWin->SetSize(500, 500);

  // Set up the lighting.
  //
  vtkSmartPointer<vtkLight> light =
    vtkSmartPointer<vtkLight>::New();
  light->SetFocalPoint(1.875,0.6125,0);
  light->SetPosition(0.875,1.6125,1);
  ren3->AddLight(light);

  ren3->GetActiveCamera()->SetFocalPoint(-10,0,-5);
  ren3->GetActiveCamera()->SetPosition(0,0,70);
  ren3->GetActiveCamera()->SetViewUp(0,1,0);
  ren3->GetActiveCamera()->SetClippingRange(20, 120);
  ren3->GetActiveCamera()->SetDistance(70);

  // This starts the event loop and invokes an initial render.
  //
  ((vtkInteractorStyle *)iren->GetInteractorStyle())->SetAutoAdjustCameraClippingRange(0);
  iren->Initialize();
  iren->Start();

  return EXIT_SUCCESS;
}




