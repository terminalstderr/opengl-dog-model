# README #

This is a toy OpenGL project that executes using a VTK framework.
The end result is a silly dog model that renders into an interactive window.

![openGlDogModel.png](https://bitbucket.org/repo/LkbqX5/images/3262926657-openGlDogModel.png)